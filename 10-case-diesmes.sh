#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
#Curs HISX
#Autor Agustin Sayar Fernandez

#Descripcio el usuari posa un numero i li contesta quins dies te

if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
	exit 1
fi





if ! [ $1 -ge 1 -a $1 -le 12 ]
then 
	echo "Error,mes $1 no valid"
	echo "Usage: $1 mes"
	exit 2
fi

mes=$1

case "$1" in
	"2")
		echo "mes:$1 te 28 dias";;
	"4"|"6"|"9"|"11")
		echo "mes:$1 te 30 dias";;
	"1"|"3"|"5"|"7"|"8"|"10")
	echo "mes:$1 te 31 dias"
		exit 0
		

