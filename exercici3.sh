#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Un comptador amb el valor indicat

#1.Controlem el error d'argument
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
	exit 1
fi



#2.Codi principal
num=0
max=$1
while [ $num -le $max ]
do
	echo "$num"
	((num++))
done
exit 0




