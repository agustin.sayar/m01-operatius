#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# $ prog file
# indicar si dir és: regular, dir o link
# --------------------------------------
# si num args no es correcte plegar

if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
fi

tipus=$1

if  [ ! -e $tipus ]
then 
	echo "no existeix"


elif [ -f $tipus ]
then
	echo "es un regular file"

elif [ -d $tipus ]
then 
	echo "es un directori"
elif [ -l $tipus ]
then 
	echo "es un link"

fi

exit 0

