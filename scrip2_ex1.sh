#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Mostrar nomes 4 o mes caracters

#Controlem el error
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 comproba"
	exit 1
fi
#Codi principal
for comproba in $*
do
	echo $comproba | cut -c1-4 
done
exit
exit 0
