#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Mostra un argument linia a linia ara enumerada

#1 Controlem el error de arguments
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
	exit 1
fi

#2.Codi principal
num=1
while read -r line  
do
	echo "$num: $line"
	((num++))

done

exit 0






