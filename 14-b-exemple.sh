 #! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#exemples llista dir.sh
#sinopsis:prog dir llistar amb un simple ls

directori=$1


if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
fi

if [ ! -e $directori ]
then 
	echo "Error: no es un directori"
       echo "usage: $0 dir"
fi
dir=$1


 
exit 0
