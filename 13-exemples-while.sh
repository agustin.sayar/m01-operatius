#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# $ prog file
# exemples de bucle while
# -------------------------------------






#7)Numera i mostra en majusculas entrada estandard
num=1
while read -r line
do
	echo "$num: $line" | tr 'a-z' 'A-Z'
	((num++))
done
exit 0


#6)Mostrar stdin linia a linia fins token fi
read -r line
while [ "$line" != "FI" ]
do
	echo "$line" | tr 'a-z' 'A-Z'
      	read -r line	
done
exit 0


#5) Numerar stdin linia a linia
num=1
while read -r line  
do
	echo "$line"
	read -r line

done

exit 0



#4) Processar entrada entandard linia a linia

while read -r line #Mentre hi hagi xixa la mostrarla
do 
	echo $line
done
exit 0


#3)Iterar per la llista d'arguments

while [ -n "$1" ] # Hi ha xixa $1
do
	echo "$1 $# $*"
	shift # Desplaçament de la posicio cap a la esquerra
done
exit 0


#2)Un comptador decreixent

min=0
num=$1
while [ $num -ge $min ]
do 
	echo  "$num "
	((num--))
done
exit 0








#Un programa que mostri del 1 al 10

num=1
max=10
while [ $num -le $max  ]		
do
	echo  $num
	((num++))

done
exit 0
