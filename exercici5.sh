#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Mostrar nomes els primers 50 caracters

#Controlem el error
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
	exit 1
fi



#Codi principal

while read -r line
do
	echo $line | cut -c1-50
done

exit 0

