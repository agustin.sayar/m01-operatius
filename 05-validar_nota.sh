 #! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#Exempleif:validar nota
# $ prog edat
#-----------------------------------------------------------
#1)Si num no es correcte
if  [ $# -ne 1 ]
then 
	echo "ERROR: nota $1 de argument"
	echo "USAGE: nota no es valida"
	exit 1
fi
if  [ $nota -lt 10]
then 
	echo "ERROR:error de argument"
	echo "USAGE: $nota no pot superar de 10"
	exit 2
fi

#2)El codi del programa


if [ $nota -lt 5]
then 
	echo "Suspes"
else
	echo "Aprovat"
fi
exit 0





