#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
#Curs HISX
#Autor Agustin Sayar Fernandez
#Programa calcula la suma dels valors que introdueix
if  [ $# -eq 0 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 numero"
	exit 1
fi
suma=0
for numero in $*
do
	suma=$((suma+numero))
done
	echo "La suma del valor son: $suma"
exit 0











