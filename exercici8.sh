#! /bin/bash
#edt ASIX-M01
#Març 2023
#El programa rep el nom si existeix al sistema sino dona error
#per stderr

#Controlem si el usuari no posa usuari
if  [ $# -ne 1 ]
then 
	echo "ERROR: error $1 de argument"
	echo "USAGE: has de posar un usuari"
	exit 1
fi
#Codi principal
for noms in $*
do
grep -q "^$noms" /etc/passwd

	if [ $? -eq 0 ]
then	
	echo "$noms: existeix "
else
	echo "$noms: no existeix" >> /dev/null
	fi
done
exit 0

