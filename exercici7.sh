#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Mostrar te mes 60 caracters

#Controlem el error
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 comproba"
	exit 1
fi



#Codi principal

while read -r line
do
	comproba=$(echo "$line" | wc -c)
	if [ $comproba -gt 60 ];then
		echo $line
fi
	
done

exit 0

