#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Mostra un argument linia a linia

#1.Controlem el error
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
	exit 1
fi

#2.Codi principal
while read -r line 
do
	echo $line
done
exit 0




