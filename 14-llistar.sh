 #! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#exemples llista dir.sh
#sinopsis:prog dir llistar a

# 1) validar arguments
if [ $# -eq 0 ]
then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir..."

fi

for dir in $*
do
  if ! [ -d $dir ]
  then
    echo "Error: $dir no és un directori" >>/dev/stderr
  else
    llista_dir=$(ls $dir)
    echo "dir: $dir"
    for nom in $llista_dir
    do
      if [ -h "$dir/$nom" ]; then
        echo -e "\t$nom és un link"
      elif [ -d "$dir/$nom"  ]; then
        echo -e "\t$nom és un dir"
      elif [ -f "$dir/$nom" ]; then
        echo -e "\t$nom és un regular"
      else
       echo -e "\t$nom és una altra cosa"
      fi
    done
  fi
done
exit 0

