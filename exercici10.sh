#! /bin/bash
# @ edt ASIX-M01 Curs 2021-2022
# $ prog file
# programa procesa linia per linia numeradas
# --------------------------------------

#Controlem el error
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
fi
#Codi principal
num=1
max=$1
while read -r line
do
	if [ "$num" -le $max ]
then
	echo "$num: $line"
	((num++))	
else
	echo "$line"
fi
num=$((num++))
done
exit 0

