#! /bin/bash
# @edt ASIX-M01 Curs 2021-2022
# gener 2021
# exemples com fer el for
# ---------------------------------

#5)
#! /bin/bash
# @edt ASIX-M01 Curs 2021-2022
# gener 2021
# Descripcio: dir els dies que té un més
# Synopsis: prog mes
# ---------------------------------

#Li passem un argument i els sumi per exemple --> 2 7 39=>48




#8)Numerar por login per logins numerats

llistat=$(cut -d: -f1 /etc/passwd | sort)
contador=1

for nom in $llistat
do 
	echo "$contador: $nom"
	((contador++))
done 
exit 0

#7)Numerar els fitxers del directori actiu numerats

llistat=(ls)
contador=1
for nom in $llistat

do 
	echo "$contador: $nom"
	((contador++))
done
exit 0



#6)numerar d'arguments
contador=1
for arg in $*
do
	echo "$contador: $arg"
	contador=$((contador+1))
done
exit 0






#)5
for arg in "$@"
do 
	echo $arg
done 
exit 0

#4)Iterar i mostrar la llista d'arguments


for arg in $*
do
	echo $arg	
done
exit 0


#3)


llistat=$(ls)
for nom in $llistat
do
	echo "$nom"
done
exit 0


#2)el mateix abans

for nom in "pere marta pau anna"
do
	echo "$nom"
done 
exit 0


# 1)iterar per un conjunt d'elements

for nom in  "pere" "marta" "pau" "anna"

do
	echo "$nom"
done
exit 0
