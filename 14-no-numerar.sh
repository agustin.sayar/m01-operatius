 #! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#exemples llista dir.sh
#sinopsis:prog dir llistar amb un simple ls




if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 tipus"
fi

if [ ! -e $dir ]
then 
	echo "Error: no es un directori"
       echo "usage: $0 dir"
fi
tipus=$(ls $tipus)

for elem in $tipus
do
	if  [ ! -e "$tipus/$elem" ]
then 
		echo "no existeix"


	elif [ -f "$tipus/$elem" ]
then
		echo "es un regular file"

	elif [ -d "$tipus/$elem" ]
then 
		echo "es un directori"
	elif [ -l "$tipus/$elem" ]
then 
		echo "es un link"
else
		echo "es un altre cosa"
fi
	
	
done

 
exit 0
