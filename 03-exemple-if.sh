 #! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#Exempleif:indicar si es major de edat
# $ prog edat
#-----------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
  echo "Error: numera args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

#2)On esta la xixa
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat es menor d'edat"
elif [ $edat -lt 65 ]
then
	
	echo "edat $edat es poblacio activa"
else
	echo "edat $edat es jubilat"
fi
exit 0



