#! /bin/bash
#@edt ASIX-M01
#Març 2023
#Mostrar te mes 60 caracters

#Controlem el error
for comproba in $*
do
if [ $# -ne 1 ]
then
	echo "Error: num args incorrecte"
	echo "usage: $0 comproba"
	exit 1
fi



	echo $comproba | cut -c1-4 |  wc -l 
done
exit 0
