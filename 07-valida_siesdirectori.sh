 #! /bin/bash
#@edt ASIX-M01
#Febrer 2023
#Exempleif:
# $ prog edat
#-----------------------------------------------------------
#1)Si num no es correcte
if  [ $# -ne 1 ]
then 
	echo "ERROR: el numero de argument"
	echo "USAGE: el directori $1 no es valid"
	exit 1
fi
if  [ ! -d $1  ]
then 
	echo "ERROR:$1 no es un directori"
	echo "USAGE: $0 dir "
	exit 2
fi

#2)El codi del programa

dir=$1
ls $dir
echo "es un directori" 
exit 0





